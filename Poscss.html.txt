<!DOCTYPE html>
<html>
<head>
<style>
div.static {
  position: absolute;
  border: 3px solid #73AD21;
}
div.relative {
  position: relative;
  width: 400px;
  height: 200px;
  border: 3px solid #73AD21;
} 

div.absolute {
  position: absolute;
  top: 80px;
  right: 0;
  width: 200px;
  height: 100px;
  border: 3px solid #73AD21;
}
div.sticky {
  position: -webkit-sticky;
  position: sticky;
  top: 0;
  padding: 5px;
  background-color: #cae8ca;
  border: 2px solid #4CAF50;
}
</style>
</head>
<body>

<h2>position Properties</h2>


<div class="static">
  This div element has position: static;
</div><br><br>
<div class="relative">This div element has position: relative;
  <div class="absolute">This div element has position: absolute;</div>
</div><br><br>
<div class="sticky">I am sticky!</div>


</body>
</html>
