<!DOCTYPE html>
<html>
<body>
<style>
a:link {
  color: green;
  background-color: transparent;
  text-decoration: none;
}
a:visited {
  color: yellow;
  background-color: transparent;
  text-decoration: none;
}
a:hover {
  color: red;
  background-color: transparent;
  text-decoration: underline;
}
a:active {
  color: yellow;
  background-color: transparent;
  text-decoration: underline;
}
</style>
<h2>Html Links</h2>

Blank link:<br>
<a href="https://www.wikipedia.org/" target="_blank">Visit Wikipedia</a><br><br>
Top links:<br>
<a href="https://www.wikipedia.org/" target="_top">Visit Wikipedia</a><br><br>
Parents link:<br>
<a href="https://www.wikipedia.org/" target="_parent">Visit Wikipedia</a><br><br>

Self link:<br>
<a href="https://www.wikipedia.org/" target="_self">Visit Wikipedia</a><br>



</body>
</html>


